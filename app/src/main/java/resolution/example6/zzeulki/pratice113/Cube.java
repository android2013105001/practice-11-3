package resolution.example6.zzeulki.pratice113;

/**
 * Created by Zzeulki on 16. 11. 15..
 */
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.microedition.khronos.opengles.GL10;

public class Cube extends Obj {
    private static float vertices[] = {
            0.0f, 1.0f, 0.0f, //Top (0)
            -1.0f, -1.0f, 1.0f, //Front Left (1)
            1.0f, -1.0f, 1.0f, //Front Right (2)
            1.0f, -1.0f, -1.0f, //Back Right (3)
            -1.0f, -1.0f, -1.0f //Back Left (4)
    };
    private static float colors[] = {
            0.0f, 1.0f, 0.0f, 1.0f,
            1.0f, 0.5f, 0.0f, 1.0f,
            1.0f, 0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f, 1.0f,
            1.0f, 0.0f, 1.0f, 1.0f
    };
    private static byte indices[] = {
            0, 1, 2,
            0, 2, 3,
            0, 3, 4,
            0, 1, 4,
            1, 2, 3,
            1, 3, 4

    };
    public Cube() {
        ByteBuffer byteBuf = ByteBuffer.allocateDirect(vertices.length * 4);
        byteBuf.order(ByteOrder.nativeOrder());
        vertexBuffer = byteBuf.asFloatBuffer();
        vertexBuffer.put(vertices);
        vertexBuffer.position(0);
        byteBuf = ByteBuffer.allocateDirect(colors.length * 4);
        byteBuf.order(ByteOrder.nativeOrder());
        colorBuffer = byteBuf.asFloatBuffer();
        colorBuffer.put(colors);
        colorBuffer.position(0);
        indexBuffer = ByteBuffer.allocateDirect(indices.length);
        indexBuffer.put(indices);
        indexBuffer.position(0);
        polygonMode = GL10.GL_TRIANGLES;
        vertexLength = indices.length;
        colorVertex = true;
        indexVertex = true;
    }
}